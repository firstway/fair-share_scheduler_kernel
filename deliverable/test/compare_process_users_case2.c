#include <sched.h>
#include <stdio.h>
#include <stdlib.h> 
#include <ctype.h>
#include <assert.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>

#define KEY_LOCK    18888
#define KEY_TASK    18899
#define KEY_FINISH  19999

static uid_t EUID0 = 1000;
static uid_t EUID1 = 1002;

int call_sys17(){

    int ret  = -1;
    
    asm (
        "movl $17, %%eax;"/* syscall 17: sys_double_nice */
        "int $0x80;"
        "movl %%eax, %0;"
        :"=r"(ret)        /* output */
        : /* no input */
        :"%eax"         /* clobbered register */
    );       
    return ret;
}

static struct sembuf sem_wait,sem_signal;

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};



int semaphore_alloc (key_t key, int sem_flags)
{
    return semget (key, 1, sem_flags);
}

/* Deallocate a binary semaphore.
 * All users must have finished their jobs.
 * Returns -1 on failure. */
int semaphore_dealloc (int semid)
{
    union semun ignored_argument;
    return semctl (semid, 1, IPC_RMID, ignored_argument);
}


int semaphore_init (int semid, int v)
{
    union semun argument;
    unsigned short values[1];
    values[0] = v;
    argument.array = values;
    return semctl (semid, 0, SETALL, argument);
}

int get_shm(size_t s){
    int shm_id;
    char* tmp_name = "/tmp";
    key_t key = ftok(tmp_name,0x11+s);
    #ifdef DEBUG
    printf("key=%x\n",key) ;
    #endif
    shm_id = shmget(key, s ,IPC_CREAT|0777); 
    if(shm_id==-1)
    {
    perror("shmget error");
    return -1;
    }
    #ifdef DEBUG
    printf("shm_id=%d\n", shm_id) ;
    #endif
    return shm_id;
};


int subprocess(int sem_id_task, int shmid, int num){

    /* Do jobs in the child process */
    pid_t pid = getpid();
    if(num==1){
        if (setuid(EUID1)) {
            printf ("Couldn't set child UID to %d.  You must run this program as root.\n", EUID1);
            return -1;
        }
    }
    else{
        if (setuid(EUID0)) {
            printf ("Couldn't set child UID to %d.  You must run this program as root.\n", EUID0);
            return -1;
        }
    }
    
    if(num==3){
        call_sys17();
        fprintf(stderr, "child %d enter, sched policy:%d, switch to uid=%d, euid=%d, will double it's share\n", pid, sched_getscheduler(pid),getuid(), geteuid());
    }else{
        fprintf(stderr, "child %d enter, sched policy:%d, switch to uid=%d, euid=%d\n", pid, sched_getscheduler(pid),getuid(), geteuid());
    }
    char *sbuf = shmat(shmid,NULL,0);
    if(sbuf==NULL) perror("shmat");

    /* Wait for task */
    int ret = semop(sem_id_task, &sem_wait, 1);
    if(ret) perror("semop");
    int a,b,c;
    c = 0;
    int k=0;
    while(sbuf[0]>0){
        for (a=0; a<1000; a++){
            for (b=0; b<500; b++) {
                c++;
            }
        }
        k++;
    };
    fprintf(stderr, "child %d exit, k=%d\n", pid, k);
    return 0;
}

int main(int argc, char *argv[]){
    int p = 3;

    if(getuid() != 0){
        fprintf(stderr,"it's NOT root!!. I need ROOT privilege. Please run this as root!!!\n");
        return -1;
    };

    struct sched_param param;
    param.sched_priority = 50;
    if( sched_setscheduler( 0, 4, &param) != 0 )
    {
        perror("sched_setscheduler");
    }

    sem_wait.sem_num = 0;
    sem_wait.sem_op = -1;
    sem_wait.sem_flg = SEM_UNDO;

    sem_signal.sem_num = 0;
    sem_signal.sem_op = p;
    sem_signal.sem_flg = SEM_UNDO;

    int sem_id_task = semaphore_alloc(KEY_TASK, IPC_CREAT|0777);
    semaphore_init (sem_id_task, 0);

    int shmid = get_shm(64);

    /* Process in parallel */
    int i;
    char *sbuf = shmat(shmid,NULL,0);
    sbuf[0] = 1;
    for(i=0; i < p; i++){
        int childpid = fork();
        if (childpid < 0){
            fprintf(stderr, "forl error!!!\n");
            return -1;
        };
        if (childpid == 0){/* child process */
            return subprocess(sem_id_task, shmid, i+1);
        };
    }

    sleep(3);
    semop(sem_id_task, &sem_signal, 1);
    int seconds = 9;
    printf("waiting %d seconds....\n", seconds);
    sleep(seconds);
    sbuf[0]=0;
    sleep(1);
};



