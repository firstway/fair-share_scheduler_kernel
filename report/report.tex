\documentclass[twocolumn]{article}
\usepackage{fullpage, parskip, amsmath, amssymb, amsthm, enumitem, hyperref, graphicx, cprotect}
\title{Homework 2: Fair-share Thread Scheduler}
\author{Biao Li (154000811, netid: bl389) and Chen Cong (151009307, netid: cc1156)}
\date{Oct 30, 2014}

\begin{document}
\maketitle

In this project, we designed and implemented the Fair-share Thread Scheduler.
Under this policy, all users in the system equally share the machine's CPU time, and all processes of a user equally share the user's CPU time.
For a process, we provided a system call to let it double its share of CPU time.

Our work is running on Biao Li's virtual machine, his netid is \verb|bl389|, the VM name is: \verb|cs416-grp36-2|.

\section{Mechanism}

The mechanism of our Fair-share scheduler consists of three main parts: share mechanism, recalculation of time slice, and doubleing share. In this section we will discuss them in detail.

\subsection{Share Mechanism}

We introduced a concept called \verb|share| to describe the number of shares that a user's total CPU time is divided to.
We will discuss how to use \verb|share| to decide the amount of time slice of processes in the next sub-section.

When a user creates a new process, its \verb|share| must be increased by one. So the number of \verb|shares| of a user is initially the number of processes, until some of the processes invoke \verb|sys_double_nice| to double its share. 

When a process exits, it will hand back all its shares, so that the user's \verb|share| should be decreased by the number of shares the process owns.

Also, when a process transfers to another user, it must transfers all its shares, which means the number of the old user's share must be decreased by the number of the shares the process owns, and the new user will gain these shares.

We implemented \verb|share| in \verb|sched.h| by adding a field \verb|share| in \verb|user_struct|.

\subsection{Recalculation of Time Slice}

Now we have \verb|share| of a user, we still need a way to represent the \verb|shares| owned by a process. Here we chose \verb|nice| to represent the shares owned by a process, because every process has this property, and \verb|nice| is not pre-occupied in our scheduler, and also it semantically fits our need. 

So, the process's share of the total CPU time of the user can be represented as:

$$\text{share}_\text{process} = \frac{\text{nice}_\text{process}}{\text{share}}$$

By this we implemented the dynamic distribution of a user's total CPU time, now we need to make sure all users have the same total CPU time.

We fixed the total CPU time a user owns to a CPU-related number, in our case we chose \verb|NICE_TO_TICK(0)| multiplied by a constant which was carefully chosen after many experiments. This CPU-related number will be the same to all users in the same machine, which makes all users to have the same total CPU time. By doing this, we made all users share the machine's total CPU time equally.

Now using the fixed total CPU time, the user's \verb|share|, and the process's \verb|nice|, we can get a process's running time by this function:

$$ \text{time}_\text{process} = \text{total\_time} \times \frac{\text{nice}_\text{process}}{\text{share}} $$

By this, we ensured that all users share the same CPU time of the machine, and all processes of a user share the same CPU time of the user, until some process changes its \verb|nice|.

\subsection{Doubling Share}

As discussed in the last sub-section, we used \verb|nice| of a process and \verb|share| of a user to dynamically distribute the user's total CPU time, now we can implement the syscall of doubling a process's share of time with these instruments.

We created the syscall \verb|sys_double_nice| to grant a process with doubled share of time. In this syscall, we do the following two things:

\begin{enumerate}
  \item {Double the \verb|nice| of the process.}
  \item {Increase the \verb|share| of the user by the increased share of the process.}
\end{enumerate}

After this, the user will have more \verb|shares|, and the process will have doubled time share compared to its original time share.

\section{Implementation}

To implement our new scheduler, we changed the following files of kernel:

\begin{verbatim}
include/linux/sched.h
kernel/exit.c
kernel/fork.c
kernel/sched.c
kernel/user.c
arch/i386/kernel/entry.S
test/Makefile
test/compare_process_single_user.c
test/compare_process_users_case1.c
test/compare_process_users_case2.c
.config
Makefile
\end{verbatim}

In this section we will discuss our modifications in detail.

\subsection{Fair-share Policy}

We implemented the \verb|Share| mechanism in \verb|sched.h|, \verb|exit.c|, \verb|fork.c|, \verb|user.c|, \verb|sched.c|.

In \verb|sched.h| we added the field \verb|share| in \verb|user_struct|.

In \verb|fork.c|, we increase \verb|share|'s value by one when a process is created.

In \verb|exit.c|, we decrease \verb|share|'s value by the process's \verb|nice| when a process exits. 

In \verb|user.c|, when a process is to switch uid, we decrease the value of the old uid's \verb|share| by the process's \verb|nice|, and increase the value of the new uid's \verb|share| by the process's \verb|nice|. 

In \verb|sched.c|, we recalculate the time slice of the process according to the user's \verb|share| and the process's \verb|nice|, according to the algorithm for the recalculation discussed in Section 1.

\subsection{Syscall for Doubling Share}

In \verb|entry.S| we used syscall 17 as our syscall for granting double nice.
We chose 17 because it's the placeholder for the "old break syscall", which is obsolete, so it's safer.
If we create a new system call, maybe it will be broken by some other softwares that also adds new syscalls.
If we replace other syscall placeholders, maybe it will bring unexpected behaviors.
We named our syscall as \verb|sys_double_nice|.

In \verb|sched.c| we implemented \verb|sys_double_nice| with the mechanism discussed in Section 1.

\subsection{Test Programs}

We provided three test programs:

\begin{verbatim}
compare_process_single_user.c
compare_process_users_case1.c
compare_process_users_case2.c
\end{verbatim}

to show that our scheduler is correctly working as expected.

The mechanism, usage and result of these three test programs will be discussed in detail in Section 3.

\subsection{Make Related}

In \verb|Makefile|, we changed the \verb|EXTRAVERSION| to sign our version of kernel.

In \verb|.config|, we included some drivers for the new kernel to properly use network.

\section{Experiments}

Our test programs simply gives every process a counter, then increases the counter continuously, then the more time share a process has, the larger the final value of its counter will be. We used semaphores and shared memory to make sure all processes begin at the same time, and also stop at the same time.

To run the experiments, it's necessary to be under \verb|root| user, then just go to the directory \verb|deliverable/test| (here \verb|deliverable| is the directory you get after unzipping \verb|deliverable.tar.gz|), then run \verb|make|, then we will have three executables: \verb|single_user|, \verb|multiple_users_1|, and \verb|multiple_users_2|. You can run them to perform the following three experiments:

\begin{enumerate}
  \item {\verb|single_user|. This experiment shows that all the processes share the total CPU time of a user equally, which means our Fair-share policy is working as designed; it also shows our new syscall (syscall 17, \verb|sys_double_nice|) is working as designed.}
  \item {\verb|multiple_users_1|. This experiment shows that if there are multiple users, every user owns the same total CPU time.}
  \item {\verb|multiple_users_2|. This experiment shows that our syscall \verb|sys_double_nice| is working as designed when there are multiple users, i.e., every user owns the same total CPU time, and when a process calls \verb|sys_double_nice|, it will double its share inside the user's CPU time.}
\end{enumerate}

\subsection{Single User}

The result of \verb|single_user| we got is shown in Figure \ref{fig:single_user}.

\begin{figure}
  \centering \includegraphics[width=0.45\textwidth]{single_user}
  \cprotect\caption{The result of \verb|single_user|}
  \label{fig:single_user}
\end{figure}

In this experiment, \verb|Process #669| called \verb|sys_double_nice|, and then the final value of its counter is about 2 times of all its peers, which means \verb|Process #669| gets double running time. This shows our syscall for doubling time share is working properly.

We can also see that other processes own almost the same CPU time, this means our Fair-share scheduler works properly to grant all processes of a user with the same CPU time share, unless they call \verb|sys_double_nice|.

\subsection{Multiple Users Case 1}

The result of \verb|multiple_users_1| we got is shown in Figure \ref{fig:multiple_users_1}.

\begin{figure}
  \centering \includegraphics[width=0.45\textwidth]{mult_1}
  \cprotect\caption{The result of \verb|multiple_users_1|}
  \label{fig:multiple_users_1}
\end{figure}

In this experiment, we have two users: \verb|User #1000| and \verb|User #1002|. \verb|User #1000| has three processes, and \verb|User #1002| has two processes.

We can see all processes of a specific user share the CPU time of that user equally.
We can also see that the total CPU time these two users own are basically the same.
This means our Fair-share scheduler works properly to grant all users with the same CPU time share.

\subsection{Multiple Users Case 2}

The result of \verb|multiple_users_2| we got is shown in Figure \ref{fig:multiple_users_2}.

\begin{figure}
  \centering \includegraphics[width=0.45\textwidth]{mult_2}
  \cprotect\caption{The result of \verb|multiple_users_2|}
  \label{fig:multiple_users_2}
\end{figure}

In this experiment, we have two users: \verb|User #1000| and \verb|User #1002|. \verb|User #1000| has two processes, and \verb|User #1002| has one process.

We can see \verb|Process #682| called \verb|sys_double_nice|, so its time share is 2 times of its peer \verb|Process #681|.
We can also see that the total CPU time these two users own are basically the same.

This means our Fair-share scheduler and our syscall \verb|sys_double_nice| are all working properly to meet the requirement.

\section{Summary}

The Fair-share schduler we designed and implemented in this project will ensure that every user in the system has an equal share of the machine's CPU time, and every process of a user also has an equal share of the user's CPU time.
We also provided a system call to let a process to double its share of CPU time.
According to the result of our three experiments, we are confident that our scheduler is working properly as expected, and can satisfy all the requirements listed in the description of the assignment.

\end{document}