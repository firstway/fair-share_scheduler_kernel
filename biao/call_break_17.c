


#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <stdio.h>

#include <stdlib.h> 


void call_sys_test(char* s){

    int ret  = -1;
    int len = sizeof(s);
    
    asm (
        "movl $17, %%eax;"/* sys break 17*/
        "int $0x80;"
        /*"movl %%eax, %0;"*/
        "movl %%eax, %0;"
        :"=r"(ret)        /* output */
        :"g"(s),"g"(len) /*  no input */
        :"%eax","%ebx","%ecx","%edx"         /* clobbered register */
    );       
    printf("return=%d\n", ret);
}

int main(){
   call_sys_test("hehhehehehe");
   return 0;
};

