


#define _GNU_SOURCE             /* See feature_test_macros(7) */
#include <sched.h>
#include <stdio.h>

#include <stdlib.h> 


void call_sys_test(char* s){

    int ret  = -1;
    int len = sizeof(s);
    
    asm (
        /* "movl $4, %%eax;" sys_write*/
        "movl $1, %%ebx;" /* stdout */
        "movl %1, %%ecx;" /* buf */
        "movl %2, %%edx;" /* buf size */
        "movl $20, %%eax;"/* sys_getpid*/
        "int $0x80;"
        /*"movl %%eax, %0;"*/
        "movl %%eax, %0;"
        :"=r"(ret)        /* output */
        :"g"(s),"g"(len) /*  no input */
        :"%eax","%ebx","%ecx","%edx"         /* clobbered register */
    );       
    printf("read count=%d\n", ret);
}

int main(){
   call_sys_test("hehhehehehe");
   return 0;
};

